# Script for intgeracting in the QGIS console to create custom shape objects
# To run this, copy and paste it into the QGIS Python Console

from PyQt5.QtCore import *

from qgis.core import (
  QgsGeometry,
  QgsPoint,
  QgsPointXY,
  QgsWkbTypes,
  QgsProject,
  QgsFeatureRequest,
  QgsDistanceArea
)
import json

layer = iface.activeLayer()

# Delete features
while layer.featureCount() > 0: # Deleting a lot at once can be slow and lock up QGIS. To account for this I'm looping until the featureCount == 0
    with edit(layer):
        listOfIds = [feat.id() for feat in layer.getFeatures()][0:1000]
        layer.deleteFeatures(listOfIds)

# print(gLine)
def create_qgs_geometry_object(xy_list):
    qgs_point_list = []
    for coord in xy_list['xy_array']:
        qgs_point_list.append(QgsPointXY(coord[0], coord[1]))
    return QgsGeometry.fromPolygonXY([qgs_point_list])

# gPolygon = create_qgs_geometry_object(xy_list)
# # print(gPolygon)
#
# feature = QgsFeature()
# feature.setGeometry(QgsGeometry.fromPolygonXY(gPolygon.asPolygon()))
# (res, outFeats) = layer.dataProvider().addFeatures([feature])

with open(r'C:\Users\mdassow\Documents\qgis\src\layers\FM_service_contour_current\export\home\radioTVdata\results\FM_service_contour_current JSON_OUTPUT.json') as json_file:
    xy_lists = json.load(json_file)

# Start an undo block
layer.beginEditCommand( 'Creating all features' )

features = []
for xy_list in xy_lists[:1000]:
    gPolygon = create_qgs_geometry_object(xy_list)
    # print(gPolygon)
    feature = QgsFeature()
    feature.setGeometry(QgsGeometry.fromPolygonXY(gPolygon.asPolygon()))
    features.append(feature)
    if len(features) == 1000:
        (res, outFeats) = layer.dataProvider().addFeatures(features)
        features = []
(res, outFeats) = layer.dataProvider().addFeatures(features)

# End the undo block
layer.endEditCommand()

