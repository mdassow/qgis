import pandas as pd

fp = r"C:\Users\mdassow\Documents\QGIS\src\projs\State_Heat_Map_Sales_Data\us-state-ansi-fips.csv"
df = pd.read_csv(fp)
df['stusps'] = df['stusps'].str.strip()
df['st'] = df['st'].astype(str).str.strip()

df[['stname','st','stusps']].to_csv(fp, index=False)