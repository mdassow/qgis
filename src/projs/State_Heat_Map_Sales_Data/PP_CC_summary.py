import pandas as pd


df = pd.read_csv(r'PP_CC.csv', encoding='latin-1')
fields = '2019 Cash Deposited,PrePay Calc,Financing Calc,TruChoice Saving Calc,2019 Cash Buying Power,2019 Cash Remaining,2019 Credit Deposited,2019 Credit Buying Power,2019 Credit Remaining,2019 Total Deposited,2019 Total Buying Power,2019 Prepay Remaining,Earned,Spent,Remaining'.split(',')
for field in fields:
    print(field)
    df[field] = df[field].str.replace(',','').astype(float)
df_group = df.groupby('STATE')

df_sum = df_group[fields].sum()
df_sum.to_csv('PP_CC_state_summary.csv')