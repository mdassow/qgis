import pandas as pd
import json
from tqdm import tqdm

def map_cols_array(row):
    row['xy_array'] = [(float(x),float(y)) for y, x in row[4:363].str.split(',')] # data comes in format (y,x) in source data, but must be (x,y) for qgis
    return row

# file below comes from https://www.fcc.gov/media/radio/general-info-fm-tv-maps-data
fp = r'C:\Users\mdassow\Documents\qgis\src\layers\FM_service_contour_current\export\home\radioTVdata\results\FM_service_contour_current.txt'
df = pd.read_csv(fp, delimiter='|', header=None)
# df = df.sample(n=1000)
tqdm.pandas()
df = df.progress_apply(map_cols_array, axis=1)
df.to_json(r'C:\Users\mdassow\Documents\qgis\src\layers\FM_service_contour_current\export\home\radioTVdata\results\FM_service_contour_current JSON_OUTPUT.json', orient='records')

print(df.head())
